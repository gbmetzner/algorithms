package algorithm.sorting.insertion

/**
 * Created by gustavo on 9/23/14.
 */
object Insertion extends App {

  var array = scala.collection.mutable.ArrayBuffer(1, 3, 5, 9, 8, 4, 5445, 65, 564, 5, 15456, 456, 456, 456, 5, 55, 6, 6, 5, 4, 99, 1, 3, 3, 5, 9, 9, 1)

  val originalSize = array.size

  for (j <- 1 until array.size) {

    val item = array(j)

    var i = j

    while (i >= 0 && array(i - 1) > item) {
      array(i) = array(i - 1)
      i = i - 1
    }

    array(i) = item
  }


  val list = List(1, 3, 5, 9, 8, 4, 5445, 65, 564, 5, 15456, 456, 456, 456, 5, 55, 6, 6, 5, 4, 99, 1, 3, 3, 5, 9, 9, 1)

  def insertionSort[A](la: List[A])(implicit ord: Ordering[A]): List[A] = {
    def insert(la: List[A], a: A) = {
      val (h, t) = la.span(ord.lt(_, a))
      h ::: (a :: t)
    }

    la.foldLeft(List[A]()) { (acc, a) => insert(acc, a)}
  }

  println(array mkString ", ")
  println(insertionSort(list).mkString(", "))
  println(list.sortWith((a, b) => a < b).mkString(", "))

}
